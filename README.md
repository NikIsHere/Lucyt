# Lucyt

*a opengl game engine with 2d and 3d rendering* 

# Compilation

1. install cmake
2. `cmake .`
3. move glfw.lib (rename) into path
4. `cmake --build .` 

:smile:

# ToDo 🧷

## Priority
- [x] CMake Build System
- [x] Camera & Input Handling | NOT FINISHED
- [ ] Lighting
- [ ] 3D Model Handling
- [x] Dear ImGUI
- [ ] Rich debugging and logging
- [ ] Post Processing
- [ ] Code Restructuring and Refactoring
- [ ] Discord Rich Presence Integration
- [ ] Vulkan layer
- [ ] lots more

### Optional

- [ ] DirectX
