#pragma once

#include <glad/glad.h>
#include "VBO.h"

class VAO
{

	public:
		GLuint ID;
		VAO();

		void LinkAttrib(VBO& VBO, GLuint Layout, GLuint numComponents, GLenum type, GLsizei stride, void* offset);
		void Bind();
		void Unbind();
		void Delete();

};

