#pragma once

#include <glad/glad.h>
#include "stb.h"
#include "shaderClass.h"

class textureClass
{

public:
	textureClass(const char* file, GLenum slot, GLenum img_type, GLenum color_format, GLenum input_type);

	GLuint ID;
	int widthImg, heightImg, numColorCh;
	GLenum img_type;

	void texUnit(Shader& shader, const char* uniform, GLuint unit);

	void Bind();
	void Unbind();
	void Delete();

};

