#include "camera.h"


camera::camera(int width, int height, glm::vec3 position) {
	this->width = width;
	this->height = height;
	
	this->Position = position;
}

void camera::matrix(float FOVdeg, float nearPlane, float farPlane, Shader& shader, const char* uniform) {

	glm::mat4 view = glm::mat4(1.0f);
	glm::mat4 projection = glm::mat4(1.0f);

	//Position = glm::vec3(x_pos, y_pos, z_pos);

	view = glm::lookAt(Position, Position + Orientation, Up);
	projection = glm::perspective(glm::radians(FOVdeg), (float)width / height, nearPlane, farPlane);

	glUniformMatrix4fv(glGetUniformLocation(shader.ID, uniform), 1, GL_FALSE, glm::value_ptr(projection * view));

}

void camera::inputs(GLFWwindow* window, float delta) {

	if (glfwGetKey(window, GLFW_KEY_W)) {
		//z_pos -= cam_speed * delta;
		Position += speed * Orientation;
	}
	if (glfwGetKey(window, GLFW_KEY_S)) {
		//z_pos += cam_speed * delta;
		Position += speed * -Orientation;
	}
	if (glfwGetKey(window, GLFW_KEY_A)) {
		//x_pos -= cam_speed * delta;
		Position += speed * -glm::normalize(glm::cross(Orientation, Up));
	}
	if (glfwGetKey(window, GLFW_KEY_D)) {
		//x_pos += cam_speed * delta;
		Position += speed * glm::normalize(glm::cross(Orientation, Up));
	}
	if (glfwGetKey(window, GLFW_KEY_SPACE)) {
		//y_pos += cam_speed * delta;
		Position += speed * Up;
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT)) {
		//y_pos -= cam_speed * delta;
		Position += speed * -Up;
	}

	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_RELEASE) 
	{



		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
		/*
		if (firstClick) {
			glfwSetCursorPos(window, width / 2, height / 2);
			firstClick = false;
		}
		*/
		double mouseX, mouseY;
		glfwGetCursorPos(window, &mouseX, &mouseY);

		float rotX = sensitivity * (float)(mouseY - (height / 2)) / height;
		float rotY = sensitivity * (float)(mouseX - (width / 2)) / width;

		glm::vec3 newOrientation = glm::rotate(Orientation, glm::radians(-rotX), glm::normalize(glm::cross(Orientation, Up)));

		if (!((glm::angle(newOrientation, Up) <= glm::radians(5.0f)) || (glm::angle(newOrientation, -Up) <= glm::radians(5.0f)))) {
			Orientation = newOrientation;
		}

		Orientation = glm::rotate(Orientation, glm::radians(-rotY), Up);
		glfwSetCursorPos(window, width / 2, height / 2);

	}
	else {
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}

}