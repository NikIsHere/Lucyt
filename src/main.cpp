#include <iostream>

#include <iostream>

//imgui

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"


#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "VAO.h"
#include "VBO.h"
#include "EBO.h"
#include "shaderClass.h"
#include "textureClass.h"
#include "camera.h"




using namespace std;

#define engineName "Lucyt"
#define WIDTH 1000
#define HEIGHT 1000



int main() {
	
	std::cout << "starting...\n";

	if (!glfwInit()) {
		std::cout << "couldnt init glfw\n";
		throw runtime_error("couldnt init glfw");
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	
	/*
	GLfloat vertices[] = 
	{
		-0.5f,	 0.0f, 0.5f,	1.0f, 0.0f, 0.0f,	0.0f, 0.0f,
		-0.5f,	 0.0f, -0.5f,	0.0f, 1.0f, 0.0f,	0.0f, 1.0f,
		 0.5f,	 0.0f, -0.5f,	0.0f, 0.0f, 1.0f,	1.0f, 1.0f,
		 0.5f,	 0.0f, 0.5f,	1.0f, 1.0f, 1.0f,	1.0f, 0.0f,
		 0.0f,	 0.8f, 0.0f,	1.0f, 1.0f, 1.0f,	1.0f, 0.0f,

	};

	GLuint indices[] =
	{
		0, 1, 2,
		0, 2, 3,
		0, 1, 4,
		1, 2, 4,
		2, 3, 4,
		3, 0, 4,
	};
	*/

	GLfloat vertices[] = 
	{
		-0.5f,	 0.0f, 0.5f,	1.0f, 0.0f, 0.0f,	0.0f, 0.0f,
		-0.5f,	 0.0f, -0.5f,	0.0f, 1.0f, 0.0f,	0.0f, 0.0f,
		 0.5f,	 0.0f, -0.5f,	0.0f, 0.0f, 1.0f,	0.0f, 1.0f,
		 0.5f,	 0.0f, 0.5f,	1.0f, 1.0f, 1.0f,	1.0f, 0.0f,

		 -0.5f,	 1.0f, 0.5f,	1.0f, 0.0f, 0.0f,	0.0f, 0.0f,
		-0.5f,	 1.0f, -0.5f,	0.0f, 1.0f, 0.0f,	0.0f, 0.0f,
		 0.5f,	 1.0f, -0.5f,	0.0f, 0.0f, 1.0f,	0.0f, 0.0f,
		 0.5f,	 1.0f, 0.5f,	1.0f, 1.0f, 1.0f,	1.0f, 1.0f,

	};

	GLuint indices[] =
	{
		0, 1, 2,
		0, 2, 3,

		4, 5, 6,
		4, 6, 7,

		0, 3, 7,		// front face
		0, 4, 7,

		0, 1, 5,		// left face
		0, 4, 5,

		1, 2, 6,		// back face
		1, 5, 6,

		3, 2, 6,		// right face
		3, 7, 6,

	};





	GLFWwindow* window;
	window = glfwCreateWindow(WIDTH, HEIGHT, engineName, NULL, NULL);
	if (window == NULL) {
		std::cout << "couldnt init glfw window\n";
		glfwTerminate();
		throw runtime_error("failed to create window");
	}
	
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);
	gladLoadGL();

	glViewport(0, 0, WIDTH, HEIGHT);

	Shader ShaderProgram("resources/shaders/default.vert", "resources/shaders/default.frag");

	VAO VAO1;
	VAO1.Bind();

	VBO VBO1(vertices, sizeof(vertices));
	EBO EBO1(indices, sizeof(indices));


	VAO1.LinkAttrib(VBO1, 0, 3, GL_FLOAT, 8*sizeof(float), (void*)0);
	VAO1.LinkAttrib(VBO1, 1, 3, GL_FLOAT, 8*sizeof(float), (void*)(3 * sizeof(float)));
	VAO1.LinkAttrib(VBO1, 2, 2, GL_FLOAT, 8*sizeof(float), (void*)(6 * sizeof(float)));
	VAO1.Unbind();
	VBO1.Unbind();
	EBO1.Unbind();


	textureClass texture("resources/textures/test.png", GL_TEXTURE0, GL_TEXTURE_2D, GL_RGB, GL_UNSIGNED_BYTE);
	

	
	//imgui

	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	ImGui::StyleColorsDark();
	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init("#version 330");

	bool debugMenu = false;

	float rotation = 0.0f;
	double fpsPrevTime = glfwGetTime();
	double prevTime = glfwGetTime();
	
	float x_pos = 0.0f;
	float y_pos = -0.5f;
	float z_pos = -2.0f;
	

	float cam_speed = 2.0f;
	float mod_rot_speed = 0.05f;

	float fov = 45.0f;

	glEnable(GL_DEPTH_TEST);

	int fps = 0;
	int fpsCount = 0;

	bool unlockFps = false;

	float bgCol[4] = { 0.07f, 0.13f, 0.17f, 1.0f };


	camera Camera(WIDTH, HEIGHT, glm::vec3(0.0f, 0.0f, 2.0f));


	while (!glfwWindowShouldClose(window)) {
		//glClearColor(0.07f, 0.13f, 0.17f, 1.0f);
		glClearColor(bgCol[0], bgCol[1], bgCol[2], 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();




		ShaderProgram.Activate();

		//update:
		
		double crntTime = glfwGetTime();
		double delta = crntTime - prevTime;
		prevTime = crntTime;
		//cout << delta << "\n";

		rotation += mod_rot_speed * delta;
		


		
		//fps calc:

		if (unlockFps) {
			glfwSwapInterval(0);
		}
		else {
			glfwSwapInterval(1);
		}

		double fpsCrntTime = glfwGetTime();
		if (fpsCrntTime - fpsPrevTime < 1.0f) {
			rotation += mod_rot_speed;
			fps++;

		}
		else {
			fpsPrevTime = fpsCrntTime;
			fpsCount = fps;
			fps = 0;
		}

		// matrices manipulation also projection




		// imgui:

		ImGui::BeginMainMenuBar();
		if (ImGui::MenuItem("Exit")) {
			break;
		}
		if (ImGui::MenuItem("Debug...")) {
			debugMenu = !debugMenu;
		}
		ImGui::EndMainMenuBar();

		if (debugMenu) {
			ImGui::Begin("Lucyt Debug Controls");
			ImGui::Text("Lucyt by Nycz");
			ImGui::Text("FPS: %i", fpsCount);
			ImGui::Checkbox("Unlock Fps", &unlockFps);
			ImGui::SliderFloat("Field of View", &fov, 0.0f, 180.0f);
			ImGui::SliderFloat("Camera Speed", &Camera.speed, 0.0f, 50.0f);
			ImGui::SliderFloat("Model Rotation Speed", &mod_rot_speed, 0.0f, 2.0f);
			ImGui::ColorEdit4("Background Color", bgCol);
			ImGui::TextColored(ImVec4(1, 1, 0, 1), "Debug info:");
			ImGui::Text("xPos %f, yPos %f, zPos %f", Camera.Position.x, Camera.Position.y, Camera.Position.z);
			ImGui::Text("Delta Time: %f", delta);
			ImGui::End();
		}
		
		if (!io.WantCaptureMouse) {
			Camera.inputs(window, delta);
		}
		Camera.matrix(fov, 0.1f, 100.0f, ShaderProgram, "camMatrix");

		texture.Bind();
		VAO1.Bind();
		glDrawElements(GL_TRIANGLES, sizeof(indices) / sizeof(int), GL_UNSIGNED_INT, 0);


		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());



		glfwSwapBuffers(window);

		glfwPollEvents();
	}

	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

	VAO1.Delete();
	VBO1.Delete();
	EBO1.Delete();
	
	texture.Delete();

	ShaderProgram.Delete();

	glfwDestroyWindow(window);
	glfwTerminate();
	return 0;
}