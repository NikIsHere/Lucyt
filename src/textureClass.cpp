#include "textureClass.h"

textureClass::textureClass(const char* file, GLenum slot, GLenum img_type, GLenum color_format, GLenum input_type)
{
	this->img_type = img_type;

	stbi_set_flip_vertically_on_load(true);												// make img load bottom up like opengl does
	unsigned char* bytes = stbi_load(file, &widthImg, &heightImg, &numColorCh, 0);		// load img with help of stbi lib

	glGenTextures(1, &ID);																// generate texture buffer
	glActiveTexture(slot);																// set batch/slot as active
	glBindTexture(img_type, ID);														// bind texture to active batch/slot

	glTexParameteri(img_type, GL_TEXTURE_MIN_FILTER, GL_NEAREST);						// set resizing mode						
	glTexParameteri(img_type, GL_TEXTURE_MAG_FILTER, GL_NEAREST);						// ..

	glTexParameteri(img_type, GL_TEXTURE_WRAP_S, GL_REPEAT);							// set repeat mode
	glTexParameteri(img_type, GL_TEXTURE_WRAP_T, GL_REPEAT);							// ..

	glTexImage2D(img_type, 0, GL_RGBA, widthImg, heightImg, 0, color_format, input_type, bytes);	// upload texture to gpu
	
	
	glGenerateMipmap(img_type);															// generate different sizes of img

	stbi_image_free(bytes);																// free memory

	Unbind();
}

void textureClass::texUnit(Shader& shader, const char* uniform, GLuint unit)
{
	GLuint texUni = glGetUniformLocation(shader.ID, uniform);
	shader.Activate();
	glUniform1i(texUni, unit);
}

void textureClass::Bind()
{
	glBindTexture(img_type, ID);
}

void textureClass::Unbind()
{
	glBindTexture(img_type, 0);
}

void textureClass::Delete()
{
	glDeleteTextures(1, &ID);
}
