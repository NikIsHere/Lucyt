#include "shaderClass.h"

std::string get_file_contents(const char* filename)
{
	std::ifstream in(filename, std::ios::binary);
	if (in) {
		std::string contents;
		in.seekg(0, std::ios::end);
		contents.resize(in.tellg());
		in.seekg(0, std::ios::beg);
		in.read(&contents[0], contents.size());
		in.close();
		return contents;
	}

	throw(errno);
}

Shader::Shader(const char* vertexFile, const char* fragmentFile)
{
	std::string vertexCode = get_file_contents(vertexFile);				// load vert shader code
	std::string fragmentCode = get_file_contents(fragmentFile);			// load frag shader code

	const char* vertexSource = vertexCode.c_str();						// convert to char arr
	const char* fragmentSource = fragmentCode.c_str();					// ..

	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);				// create vert shader buffer
	glShaderSource(vertexShader, 1, &vertexSource, NULL);				// set source code of shader buffer to vert code
	glCompileShader(vertexShader);										// compile vert shader

	compileErrors(vertexShader, "vertex");

	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);			// create frag shader buffer
	glShaderSource(fragmentShader, 1, &fragmentSource, NULL);			// set source code of shader buffer to frag code
	glCompileShader(fragmentShader);									// compile frag shader

	compileErrors(fragmentShader, "fragment");							// helper function for debugging

	ID = glCreateProgram();												// create shader program on shader program buffer

	glAttachShader(ID, vertexShader);									// attach vert shader to program
	glAttachShader(ID, fragmentShader);									// attach frag shader to program

	glLinkProgram(ID);													// link shader program with gpu

	compileErrors(ID, "program");										// helper function for debugging

	glDeleteShader(vertexShader);										// free memory
	glDeleteShader(fragmentShader);										// ..


}

void Shader::Activate()
{
	glUseProgram(ID);
}

void Shader::Delete()
{
	glDeleteProgram(ID);
}

void Shader::compileErrors(unsigned int shader, const char* type)	// TODO: subject to change
{
	GLint hasCompiled;
	char infoLog[1024];
	
	if (type != "program") {
		glGetShaderiv(shader, GL_COMPILE_STATUS, &hasCompiled);
		if (hasCompiled == GL_FALSE) {
			glGetShaderInfoLog(shader, 1024, NULL, infoLog);
			std::cout << "SHADER_COMPILATION_ERROR for: " << type << infoLog << "\n";
		}
	}
	else {
		glGetProgramiv(shader, GL_LINK_STATUS, &hasCompiled);
		if (hasCompiled == GL_FALSE) {
			glGetProgramInfoLog(shader, 1024, NULL, infoLog);
			std::cout << "SHADER_LINKING_ERROR for: " << type << infoLog <<"\n";
		}
	}
}
